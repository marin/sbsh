(asdf:defsystem :sbcl-shell
  :name "SBCL-SHELL"
  :version "0.0.1"
  :description "A classic fork/exec shell using SBCL"
  :serial t
  :depends-on (#:cl-ppcre
               #:alexandria
               #:cffi
               #:cl-readline
               #:unix-opts
               #:slynk)
  :components ((:file "package")
               (:file "utilities")
               (:file "shell-output")
               (:file "read-token")
               (:file "pipeline")
               (:file "builtins-and-aliases")
               (:file "shell")))
