
(in-package :sbcl-shell)

(defvar *lisp-results-writer* (lambda (stream el)
                                (format stream "~A~&" el)))

(defvar *builtins-in-main-shell* nil)

(defvar *in-child-process* nil)

(defvar *child-pids* ())

;; LOAD EXEC SHARED LIBRARY

(defun find-execute-library ()
  (make-pathname :directory (pathname-directory (sb-posix:readlink "/proc/self/exe"))
                 :name "sbsh_lib"))

(define-alien-routine ("execv_for_sbcl" shared-execv) int
  (program sb-alien:c-string)
  (args (* sb-alien:c-string)))

(define-alien-routine ("execvp_for_sbcl" shared-execvp) int
  (program sb-alien:c-string)
  (args (* sb-alien:c-string)))

(define-alien-routine ("sb_tcgetpgrp" shared-tcgetpgrp) int
  (fd int))

(define-alien-routine ("sb_tcsetpgrp" shared-tcsetpgrp) int
  (fd int)
  (pid int))

(define-alien-routine ("sb_ignore_signals" shared-set-signals) void
  (ig int))

(defun set-ignore-signals (&optional (ignore-signals t))
  (if ignore-signals
      (shared-set-signals 1)
      (shared-set-signals 0)))

;;; MACROS AND UTILITIES

;; (defmacro nullify (&rest places)
;;   `(setf ,@(mapcan (lambda (place)
;;                      `(,place nil))
;;                    places)))

(defmacro with-readtable-case ((readtable-case &optional (reset t)) &body body)
  (let ((r (gensym "SAVED-CASE")))
    `(let ((,r (readtable-case *readtable*)))
       (unwind-protect
            (progn
              (setf (readtable-case *readtable*) ,readtable-case)
              (locally ,@body))
         (when ,reset
           (setf (readtable-case *readtable*) ,r))))))

(defmacro aif (condition then &optional else)
  `(let ((it ,condition))
     (if it
         ,then
         ,else)))

(defmacro with-args ((var str-list) &body body)
  (let ((sap (gensym "SAP"))
        (size (gensym "SIZE")))
    `(multiple-value-bind (,sap ,var ,size)
         (sb-impl::string-list-to-c-strvec ,str-list)
       (unwind-protect (progn ,@body)
         (sb-impl::deallocate-system-memory ,sap ,size)))))

(defmacro with-pipe ((read-var write-var &optional filedes2) &body body)
  `(multiple-value-bind (,read-var ,write-var)
       (sb-posix:pipe ,@(when filedes2 `(,filedes2)))
     ,@body))

(defun call-with-child-process (call-in-child call-in-parent)
  (let ((pid (sb-posix:fork)))
    (if (= pid 0)
        (funcall call-in-child pid)
        (progn
          (push pid *child-pids*)
          (funcall call-in-parent pid)))))

(defmacro with-child-process ((pid) &body (child parent))
  (let ((c (gensym "CHILD"))
        (p (gensym "PARENT")))
    `(let ((*in-child-process* t))
       (flet ((,c (,pid) (declare (ignorable ,pid)) ,child)
              (,p (,pid) (declare (ignorable ,pid)) ,parent))
         (declare (dynamic-extent #',c #',p))
         (call-with-child-process #',c #',p)))))

(defmacro do-in-child-process (&body body)
  (let ((pid (gensym "PID")))
    `(with-child-process (,pid)
       (progn ,@body)
       ,pid)))

(defun ensure-closed (fd)
  (handler-case (sb-posix:close fd)
    (sb-posix:syscall-error (c)
      (declare (ignorable c))
      nil)))

(defun ensure-stream (fd-or-stream)
  (if (sb-sys:fd-stream-p fd-or-stream)
      fd-or-stream
      (sb-sys:make-fd-stream fd-or-stream)))

(defun read-until-delimiter (stream delimiter)
  (let ((l nil))
    (loop for c = (read-char stream nil nil)
          if (or (null c) (and (characterp delimiter)
                               (char= c delimiter)))
            do (return-from read-until-delimiter (coerce (reverse l) 'string))
          else
            do (push c l))))

(defun read-list (stream)
  (let ((thelist nil))
    (loop for c = (peek-char nil stream nil nil)
          if (null c)
            do (return-from read-list (reverse thelist))
          else
            do (push (read stream) thelist))))

(defun set-env-variable (variable)
  (let ((spec (cl-ppcre:split "(=)" variable :omit-unmatched-p t
                                             :with-registers-p t)))
    (if (> (length spec) 1)
        (destructuring-bind (variable eq &rest rest) spec
          (declare (ignore eq))
          (setf (uiop:getenv variable)
                (expand-for-$ (apply #'concatenate 'string rest))))
        (dfmt '(:shell-variable :error :syntax-error)
              "incorrect shell variable specification ~S~&" variable))))

(defgeneric exec-for-keyword (keyword full-spec read write close-in-child shell))

(defmethod exec-for-keyword (key full read write close shell)
  (declare (ignore key read write close shell))
  (error 'invalid-shell-spec-error :spec full))

(defmacro defspec-exec (keyword (&optional (spec 'spec) (read 'read) (write 'write)
                                   (close-in-child 'close-in-child)
                                   (shell-vars 'shell-vars))
                        (&key (install-var-fn 'install-shell-variables)
                           autoinstall-shell-vars)
                        &body body)
  (multiple-value-bind (var key)
      (if (consp keyword)
          (values (car keyword) (cadr keyword))
          (values (gensym) keyword))
    `(defmethod exec-for-keyword
         ((,var (eql ,key)) ,spec ,read ,write ,close-in-child ,shell-vars)
       ,@(if install-var-fn
             `((flet ((,install-var-fn (variables)
                        (map nil 'set-env-variable variables)))
                 ,@(when autoinstall-shell-vars
                     `((,install-var-fn ,shell-vars)))
                 ,@body))
             body))))

(defspec-exec :output-to-file () ()
  (do-in-child-process
    (set-ignore-signals nil)
    (map nil 'ensure-closed close-in-child)
    (ensure-closed write)
    (sb-posix:dup2 read sb-sys:*stdin*)
    (install-shell-variables shell-vars)
    (with-open-file (f (format nil "~A~A" (uiop:getcwd) (cadr spec))
                       :direction :output
                       :if-exists :supersede
                       :if-does-not-exist :create)
      (ignore-errors
       (loop (write-char (read-char sb-sys:*stdin* nil nil) f))))
    (exit :code 0)))

(defspec-exec :append-to-file () ()
  (do-in-child-process
    (set-ignore-signals nil)
    (map nil 'ensure-closed close-in-child)
    (ensure-closed write)
    (sb-posix:dup2 read sb-sys:*stdin*)
    (install-shell-variables shell-vars)
    (with-open-file (f (format nil "~A~A" (uiop:getcwd) (cadr spec))
                       :direction :output
                       :if-exists :append
                       :if-does-not-exist :create)
      (ignore-errors
       (loop (write-char (read-char sb-sys:*stdin* nil nil) f))))
    (exit :code 0)))

(defspec-exec :lisp-subshell () ()
  (do-in-child-process
    (set-ignore-signals nil)
    (map nil 'ensure-closed close-in-child)
    (sb-posix:dup2 read sb-sys:*stdin*)
    (sb-posix:dup2 write sb-sys:*stdout*)
    (install-shell-variables shell-vars)
    (let* ((*standard-input* (ensure-stream sb-sys:*stdin*))
           (*standard-output* (ensure-stream sb-sys:*stdout*))
           (result (eval (cadr spec))))
      (finish-output *standard-output*)
      (if (numberp result)
          (exit :code result)
          (exit :code (if result 0 1))))))

(defspec-exec :lisp-subshell-format-result () ()
  (do-in-child-process
      (set-ignore-signals nil)
    (map nil 'ensure-closed close-in-child)
    (sb-posix:dup2 read sb-sys:*stdin*)
    (sb-posix:dup2 write sb-sys:*stdout*)
    (install-shell-variables shell-vars)
    (let* ((string (cadr spec))
           (result (eval (caddr spec))))
      (cond ((stringp string)
             (finish-output *standard-output*) ;
             ;; We need to set the output column to zero manually, because the
             ;; newline we read during input doesnt register for the output.
             (setf (sb-impl::fd-stream-output-column sb-sys:*stdout*) 0)
             (format sb-sys:*stdout* string result))
            ((eql string 'pprint)
             (pprint result *standard-output*)
             (fresh-line *standard-output*)))
      (finish-output *standard-output*)
      (if (numberp result)
          (exit :code result)
          (exit :code (if result 0 1))))))

(defun parse-./ (string)
  (if (and (> (length string) 2)
           (char= (char string 0) #\.)
           (char= (char string 1) #\/))
      (concatenate 'string
                   (native-namestring (uiop:getcwd))
                   string)
      string))

(defun exec (spec read write &rest close-in-child)
  "Execute a single specification with a read and write fd duped to the standard
input and output."
  (dfmt :execute "~&Exec spec: ~A~&" spec)
  (let (shell-variables rest-of-spec)
    (loop named aquire-vars
          for (var . rest) on spec
          do (multiple-value-bind (beg end)
                 (ignore-errors (cl-ppcre:scan "[a-zA-Z_]*=.*" var))
               (if (and (numberp beg)
                        (numberp end)
                        (zerop beg)
                        (= end (length var)))
                   (push var shell-variables)
                   (progn (setf rest-of-spec (cons var rest))
                          (return-from aquire-vars nil)))))
    (setf shell-variables (reverse shell-variables))
    (let ((spec rest-of-spec))
      (dfmt :execute "~&shell vars: ~A, shell spec: ~A~&" shell-variables spec)
      (cond ((stringp (car spec))
             (aif (gethash (car spec) *builtin-hash*)
                  (if (and *builtins-in-main-shell*
                           (not (always-run-in-subshell-p (car spec)))
                           (not shell-variables))
                      ;; we return multiple values with second value
                      ;; :main-process if the thing has been called within the
                      ;; main process, and :sub-process if it has been called
                      ;; within a child process.
                      (handler-bind
                          ((error
                             (lambda (c)
                               (unless *debug-on-error*
                                 (format t "Error: ~A~&When applying ~A to ~A~&"
                                         c it (cdr spec))
                                 (let ((r (find-restart 'ignore-error c)))
                                   (when r
                                     (invoke-restart r)))))))
                        (values (apply it (cdr spec)) :main-process))
                      (values (do-in-child-process
                                (set-ignore-signals nil)
                                (map nil 'ensure-closed close-in-child)
                                (loop for variable in shell-variables
                                      do (set-env-variable variable))
                                (sb-posix:dup2 read sb-sys:*stdin*)
                                (sb-posix:dup2 write sb-sys:*stdout*)
                                (let ((res (apply it (cdr spec))))
                                  (dfmt '(:and :execute :builtin)
                                        "~%result of builtin ~A: ~A~%"
                                        (car spec) res)
                                  (if (numberp res)
                                      (exit :code res)
                                      (exit :code (if res 0 1)))))
                              :sub-process))
                  (do-in-child-process
                    (set-ignore-signals nil)
                    (map nil 'ensure-closed close-in-child)
                    (let ((spec (apply-alias-transformation (car spec) (cdr spec)))
                          (stdout sb-sys:*stdout*))
                      (loop for variable in shell-variables
                            do (set-env-variable variable))
                      (sb-posix:dup2 read sb-sys:*stdin*)
                      (sb-posix:dup2 write sb-sys:*stdout*)
                      (with-args (args spec)
                        (shared-execvp (parse-./ (native-namestring (car spec)))
                                       args))
                      (format stdout "Unable to execute ~A~%" spec)
                      (exit :code 1)))))
            ((keywordp (car spec))
             (let ((key (car spec)))
               (exec-for-keyword key spec read write close-in-child
                                 shell-variables)))
            (t (error "Unable to execute shell specification ~A~&" spec))))))

;;; PIPELINES

(defun process-pipes (pipes write-fd &optional accumulator)
  "Process the middle pipes of a multipipe."
  (if pipes
      (with-pipe (read write)
        (multiple-value-bind (pid builtin)
            (exec (car pipes) read write-fd write)
          (ensure-closed read)
          (process-pipes (cdr pipes)
                         write
                         (cons (list pid builtin read write-fd (car pipes))
                               accumulator))))
      (values (reverse accumulator)
              write-fd)))

(defun multipipe (first middles last)
  "Run multiple pipeline specs"
  (with-pipe (read write)
    (multiple-value-bind (pid builtin)
        (exec last read sb-sys:*stdout* write)
      (multiple-value-bind (%accumulated write-fd)
          (process-pipes (reverse middles) write)
        (declare (ignorable write-fd))
        (let* ((accumulated (append (reverse %accumulated)
                                    (list (list pid builtin read write last))))
               (proc (car accumulated)))
          ;; We have a list of accumulated processes which has the pid, the fd
          ;; they read from, and the fd they write to.
          (dfmt '(:and :pipeline :multipipe) "~&ACCUMULATED: ~A~&" accumulated)
          (destructuring-bind (ipid bi iread iwrite name) proc
            (declare (ignorable bi ipid iwrite name))
            (let ((initial-pid (exec first sb-sys:*stdin* write-fd iread)))
              (let ((st (sb-posix:waitpid initial-pid 0)))
                (finish-output (sb-sys:make-fd-stream write-fd))
                (finish-output (sb-sys:make-fd-stream iwrite))
                (ensure-closed write-fd)
                (ensure-closed iwrite)
                (ensure-closed iread)
                (cons st
                      (loop for (pid builtin read write name) in accumulated
                            collect (let ((stat (multiple-value-list
                                                 (sb-posix:waitpid pid 0))))
                                      (finish-output
                                       (sb-sys:make-fd-stream write))
                                      (ensure-closed read)
                                      (ensure-closed write)
                                      (if builtin
                                          (list (car stat) (/ (cadr stat) 256))
                                          stat))))))))))))

(defun nopipe (program)
  "Run a single pipeline spec"
  (let ((*builtins-in-main-shell* t))
    (multiple-value-bind (pid builtin)
        (exec program sb-sys:*stdin* sb-sys:*stdout*)
      ;; only when we nopipe will pid ever be a non-child pid, because of builtins
      ;; and pipable builtins.
      (dfmt '(:and :pipeline :nopipe) "~&PID RETURNED IN NOPIPE IS: ~A~&" pid)
      (cond ((eql builtin :main-process)
             pid)
            ((eql builtin :sub-process)
             (multiple-value-bind (pid code)
                 (sb-posix:waitpid pid 0)
               (declare (ignore pid))
               (/ code 256)))
            (t
             (multiple-value-bind (pid code)
                 (sb-posix:waitpid pid 0)
               (declare (ignore pid))
               code))))))

(defun break-open-pipeline (pipeline)
  "Break a pipeline into the first, middle, and last pipes"
  (let* ((initial (car pipeline))
         (%rest (cdr pipeline))
         (others (butlast %rest))
         (final (car (last %rest))))
    (values initial
            others
            final)))

(defun piper (&rest pipeline)
  "Take pipeline where every element is a list of a program and its arguments all
as strings."
  (dfmt :pipeline "~&run pipeline: ~S~&" pipeline)
  (multiple-value-bind (first mids last) (break-open-pipeline pipeline)
    (dfmt :pipeline "~&First: ~S~&Mids: ~S~&Last: ~S~&" first mids last)
    (cond ((and first last) ; two+ programs
           ;; multipipe handles any pipeline we need
           (setf *previous-cmd-exit-code*
                 (cadr (car (last (multipipe first mids last))))))
          (first ; only one program
           (setf *previous-cmd-exit-code* (nopipe first)))
          (t ; nothing to do...
           (format t "~&Nothing to do...~&")
           (values)))))

(defun call-with-fd-output-to-string (fun dup-stdout)
  "Call FUN with a stream directing to the write end of a pipe. Once FUN has
returned the output is flushed and a string is generated and returned by reading
all output from the read end of the pipe."
  (with-pipe (r w)
    (let ((istream (sb-sys:make-fd-stream r :input t))
          (pid 
            (do-in-child-process
              (when dup-stdout
                (sb-posix:dup2 w sb-sys:*stdout*))
              (let* ((ostream (sb-sys:make-fd-stream w :output t
                                                       :element-type 'character))
                     (result (funcall fun ostream)))
                (declare (ignore result))
                (finish-output ostream)
                (exit :code 0)))))
      (multiple-value-bind (pid code)
          (sb-posix:waitpid pid 0)
        (declare (ignore pid code))
        (unwind-protect (with-output-to-string (s)
                          (loop while (listen istream)
                                do (write-char (read-char istream) s)))
          (ensure-closed r)
          (ensure-closed w))))))

(defmacro with-fd-output-to-string ((var &key (dup-stdout t)) &body body)
  "Evaluate BODY with VAR bound to a stream. If DUP-STDOUT is T then standard
output will be redirected to this stream. A string is returned.

(format t \"Collected: ~A~&\"
        (with-fd-output-to-string (s)
          (piper '(\"ls\"))))"
  (let ((fn (gensym)))
    `(flet ((,fn (,var)
              ,@body))
       (declare (dynamic-extent #',fn))
       (call-with-fd-output-to-string #',fn ,dup-stdout))))
