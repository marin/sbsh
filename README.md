# SBSH

A fork+exec shell for SBCL. The goal of this project is to create a shell which
can be programmed in CL, and which can pipe input and output to/from lisp
functions. The secondary goal of this shell is to be POSIX compliant. However
this is not a priority.

This has only been tested with single threaded sbcl. Multi threaded sbcl may
work, but there may only be one thread running because `fork` wont function
otherwise.

## Building

Build SBSH with `make`. This will create a library exposing some functions, as
well as the executables `sbclsh` and `sbsh`. Run `sbclsh` to open an image, Run
`sbsh` to run the shell. The lisp image used to build everything can be
controlled with the `LISP` environment variable

## SBSH Scripts

When SBSH is invoked as a script, or with a file as the last element of its
argument list, then it enters the script loop. This reads and evaluates lisp
code from the script file. The reader macro `#!` is used to evaluate shell
lines. If there is no numeric argument or the provided numeric argument is
greater than zero, that number of valid shell lines is read. This works by
reading a line, checking to see if its valid, and if so collecting it. When zero
is passed as the numarg then shell lines are read and collected until a single
line consisting solely of an exclamation mark is read. The reader macro
generates the appropriate forms to run execute these lines.

```
#!/usr/local/bin/sbsh

#0!
touch myfile.txt
echo "hi" >> myfile.txt 
!

(with-open-file (f "myfile.txt" :direction :input)
  (loop for line = (read-line f nil nil)
        while line
        do (write-string line)
           (fresh-line)))

(with-open-file (f "myfile.txt" :direction :output
                                :if-exists :append)
  (fresh-line f)
  (write-string "EOF!" f))


#!
echo "done" >> myfile.txt
```

## Shell Usage

Run `sbsh` to open a shell. If the file `~/.sbshrc` exists it will be
loaded. This is where you should place aliases, builtins, etc. 

### Using CL in a Pipeline

Place the following in your `.sbshrc`:

```lisp
(defun collect-all-input ()
  (with-output-to-string (s)
    (handler-case (loop (write-char (read-char) s))
      (error () nil))))
```

Now in the shell enter the line:

```
$ echo "$PATH" | #&"~{~A~^~%~}"(cl-ppcre:split ":" (collect-all-input))
```

The `#&` token (inspired by but different from a regular lisp reader macro)
tells the reader to collect a sexp and execute it in a subshell, and potentially
format the output. The above example formats every entry in your PATH on a new
line. We can even pipe this into `wc` to find how many entries are in the path
(though thats a rather roundabout way of doing it):

```
$ echo "$PATH" | #&"~{~A~^~%~}"(cl-ppcre:split ":" (collect-all-input)) | wc -l
```

Lisp readers can be defined with `define-lisp-reader`. They must take a count
and a stream name, and return a list starting with a keyword to differentiate
them. See the source for more details.

## Lisp Readers

Lisp readers are introduced after a `#` character, and read in lisp code to
execute. All lisp readers accept a count, which most often refers to how many
lisp forms to read in.

### `#&`

The `#&` reader is the formatting reader. It reads in a number of forms and
formats the result of evaluation based upon a form given after reader macro
invocation. If this form is a string then that string is used as the format
string. If it is a character then a format string is constructed which consists
of that character as a format directive, followed by `~&`. If the character is
`;` then the results are not formatted.

Examples:

`$ #2&;(print 'hi) (print 'ho)`
`$ #&S(list 1 2 3)`

### Builtins and Aliases

When a pipeline is being executed, every program is first checked against the
builtins, then is transformed based upon the available aliases, and is finally
executed.

Builtins are normal lisp functions which can be invoked in the shell like a
normal program. If a builtin is called outside of a pipeline it is applied to
its arguments within the main shell process, but if it is part of a pipeline it
is applied within a subshell and can be part of the pipeline. 

Builtins may optionally have their arguments read or evaluated by supplying the
`:read` or `:eval` keyword arguments. Supplying `:eval` implies `:read`. 

Builtin examples:

```lisp
(defbuiltin ("cd" chdir) (dir)
  (uiop:chdir dir))

(defbuiltin ("exit" exit-sbcl-shell :eval t) (&optional code)
  (let ((code (or code 0)))
    (exit :code code)))

(defbuiltin ("source" source-file) (file)
  (load file))

(defbuiltin ("variable-value" varval :eval t) (variable)
  (format t "~A~&" variable))
```

An extension of builtins is "mains", defined with `defmain`. `defmain` defines a
builtin procedure with additional option processing using `unix-opts`.
Procedures defined with defmain will always be run in a subshell. The return
value of these procedures is used as the exit code if it is a number, otherwise
if it is truthy 0 is used and if it is nil 1 is used. As an example look to the
`repl` command, which defines a function called `sbsh-repl` that is accessible
via `repl` in the command line. This command reads and evaluates from standard
input until a specific token is encountered.

```lisp
(defmain ("repl" sbsh-repl) ((opts free :argv argv
                                        :options-variable defined-options)
                             (:name :help
                              :description "Print this help text"
                              :short #\h
                              :long "help")
                             (:name :exit-token
                              :description "Token used to exit REPL"
                              :short #\e
                              :long "exit-token"
                              :default "EXIT"
                              :arg-parser #'identity
                              :meta-var "TOKEN")
                             (:name :eval
                              :description
                              "a form to evaluate. When provided dont open a repl."
                              :short #\E
                              :long "eval"
                              :default "NIL"
                              :arg-parser #'identity
                              :meta-var "FORM"))
  (declare (ignore free))
  (let ((intty (= 1 (sb-unix:unix-isatty (ensure-stream-fd *standard-input*))))
        (*read-eval* nil))
    (flet ((obtain-input ()
             (when intty
               (format sb-sys:*stdout* "~A> " (package-name *package*))
               (finish-output))
             (read))
           (print-output (str res)
             (unless (string= str "")
               (write-line str)
               (fresh-line))
             (when intty
               (princ res)
               (fresh-line))))
      (cond ((getopt :help)
             (unix-opts:describe
              :prefix "Read and evaluate lisp forms"
              :usage-of "read-eval-lisp"
              :defined-options defined-options))
            ((and (getopt :eval)
                  (not (string= (getopt :eval) "NIL")))
             (let ((e (getopt :eval)))
               (eval (read-from-string e))
               (fresh-line)
               (force-output)))
            (t (let* ((*package* *package*)
                      (exit (read-from-string (getopt :exit-token))))
                 (format t "Starting REPL, exit with the token ~A~&" exit)
                 (loop for sexp = (obtain-input)
                       if (and (atom sexp)
                               (eql sexp exit))
                         do (fresh-line)
                            (return-from sbsh-repl t)
                       else
                         do (let* (res
                                   (str (with-output-to-string (*standard-output*)
                                          (setf res (handler-case (eval sexp)
                                                      (error (c) c))))))
                              (print-output str res)))))))))
```

If whatever is being executed is not a builtin then it is transformed for the
applicable alias and then executed. Aliases have a longhand and a shorthand for
definition. An alias defined with the longhand must take a list of tokenized
arguments and return a full list of tokenized arguments including the program
name.

```lisp
(defalias "ls" "ls --color=auto")
;; equivalent to
(define-alias-transformation "ls" (program &rest args)
  (declare (ignore program))
  (append (list "ls" "--color=auto") args))
```

## Prompt Customization

SBSH provides an ease of use macro for controlling the terminal with ansi escape
codes: `with-ansi-options`. This takes a variable and a stream to bind it to,
and then a list of options as keywords. If you want to write colored output or
use other escape sequences you should use this macro, as it encloses all the
escape sequences in `#\SOH` and `#\STX`. Currently only `:foreground` and
`:background` are implemented, and accept either an integer with red green and
blue values or a keyword indicating one of the terminal colors.

To change the prompt define a function which takes a stream and writes all
output to it. Then set the variable `*display-prompt-function*` to this
function. This variable will be funcalled with the appropriate stream. For
example:

```lisp
(defun my-prompt (stream)
  (with-ansi-options (s stream :foreground :red :background #x0000aa)
    (format s "SBSH$ ")))

(setf *display-prompt-function* 'my-prompt)
```
