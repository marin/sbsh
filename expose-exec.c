#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>

int execv_for_sbcl (char *prog, char **args) {
  return execv(prog, args);
}

int execvp_for_sbcl (char *prog, char **args) {
  return execvp(prog, args);
}

int sb_tcgetpgrp (int fd) {
  return tcgetpgrp(fd);
}

int sb_tcsetpgrp (int fd, int pid) {
  return tcsetpgrp(fd, pid);
}

void sb_ignore_signals(int ignore) {
  if (ignore == 1) { /* Ignore the signals */
    /* signal(SIGINT,  SIG_IGN); */
    signal(SIGQUIT, SIG_IGN);
    signal(SIGTSTP, SIG_IGN);
    signal(SIGTTIN, SIG_IGN);
    signal(SIGTTOU, SIG_IGN);
    /* signal(SIGCHLD, SIG_IGN); */
  } else { /* Dont ignore the signals */
    signal(SIGINT,  SIG_DFL);
    signal(SIGQUIT, SIG_DFL);
    signal(SIGTSTP, SIG_DFL);
    signal(SIGTTIN, SIG_DFL);
    signal(SIGTTOU, SIG_DFL);
    /* signal(SIGCHLD, SIG_DFL); */
  }
  return;
}
