(in-package :sbcl-shell)

(defvar *lisp-toggle* #\#)
(defvar *lisp-readers* (make-hash-table))
(defvar *read-package* (find-package :sbcl-shell))

(defun tokenize-for-char (string char-or-regex &key (join-using "\""))
  (let ((splits (cl-ppcre:split (if (stringp char-or-regex)
                                    char-or-regex
                                    (coerce (list char-or-regex) 'string))
                                string)))
    ;; (print splits)
    (flet ((pred (a b)
             (declare (ignore b))
             (ignore-errors (char= #\\ (uiop:last-char a))))
           (joinery (a b)
             (concatenate 'string (butlast-char a) join-using b)))
      (join #'pred #'joinery splits))))

(define-condition tokenize-count-error (error)
  ((element :initarg :element :reader tokenize-count-error-element)
   (string :initarg :string :reader tokenize-count-error-string)
   (splits :initarg :splits :reader tokenize-count-error-splits))
  (:report
   (lambda (c s)
     (format s "Expected an even number of ~S in ~S,~%which split into ~S."
             (tokenize-count-error-element c)
             (tokenize-count-error-string c)
             (tokenize-count-error-splits c)))))

(defun tokenize-single-quote (string &optional (expect-even-count t))
  (let ((splits (cl-ppcre:split "(')" string :with-registers-p t
                                             :omit-unmatched-p t)))
    (when expect-even-count
      (let ((count (count-if (geneq #'string= "'") splits)))
        (unless (evenp count) ;; could also use (when (oddp count) ...)
          (restart-case (error 'tokenize-count-error :element "'"
                                                     :string string
                                                     :splits splits)
            (ignore-error () nil)))))
    (flet ((pred (a b)
             (or (ignore-errors (char= #\\ (uiop:last-char a)))
                 (string= a "'")
                 (string= b "'")))
           (joinery (a b)
             (cond ((and (string= a "'")
                         (string= b "'"))
                    "")
                   ((string= a "'") b)
                   ((string= b "'") a)
                   (t
                    (concatenate 'string (butlast-char a) "'" b)))))
      (join #'pred #'joinery splits))))

(defun tokenize-double-quote (string &optional (expect-even-count t))
  (let ((splits (cl-ppcre:split "(\")" string :with-registers-p t
                                              :omit-unmatched-p t)))
    ;; (print splits)
    (when expect-even-count
      (let ((count (count-if (geneq #'string= "\"") splits)))
        (unless (evenp count) ;; could also use (when (oddp count) ...)
          (restart-case (error 'tokenize-count-error :element "\""
                                                     :string string
                                                     :splits splits)
            (ignore-error () nil)))))
    (flet ((pred (a b)
             (or (ignore-errors (char= #\\ (uiop:last-char a)))
                 (string= a "\"")
                 (string= b "\"")))
           (joinery (a b)
             (cond ((and (string= a "\"")
                         (string= b "\""))
                    "")
                   ((string= a "\"") b)
                   ((string= b "\"") a)
                   (t
                    (concatenate 'string (butlast-char a) "\"" b)))))
      (join #'pred #'joinery splits))))

;;; Tokenizing is done in several passes (on one shell line).
;;;
;;; 1. we tokenize for #\'. then every other element in the resulting list
;;; should not be dealt with. So we descend upon the ones that we want to deal
;;; with.
;;;
;;; 2. Now we descend further, and for every one string that we do want to deal
;;; with, we tokenize for #\". Here we need to treat every other string with
;;; expansion - eg "$PATH" needs to be expanded.
;;;
;;; 3. Now we descend one more level, and parse out for spaces. This is the
;;; final level, and after parsing out the spaces we flatten the entire tree and
;;; pass it off to whatever will run the pipeline.

(defun tokenize-line (line)
  (restart-case 
      (labels ((splitter (string)
                 (cl-ppcre:split "(\\|)|(>>)|(>)|(<<)|(<)"
                                 ;; "(\\|>>)|(\\|>)|(\\|)|(>>)|(>)|(<<)|(<)"
                                 string
                                 :omit-unmatched-p t
                                 :with-registers-p t))
               (replace-env (line)
                 (let ((splits (remove-if
                                (geneq #'string= "")
                                (cl-ppcre:split "(\\$\\?)|(\\$[a-zA-Z_]*)"
                                                line
                                                :with-registers-p t
                                                :omit-unmatched-p t))))
                   (apply #'concatenate 'string
                          (mapcar (lambda (s)
                                    (cond ((char= (char s 0) #\$)
                                           (if (char= (char s 1) #\?)
                                               (format nil "~D"
                                                       *previous-cmd-exit-code*)
                                               (uiop:getenv (subseq s 1))))
                                          ((char= (char s 0) #\~)
                                           (if (> (length s) 1)
                                               (if (char= (char s 1) #\/)
                                                   (concatenate 'string
                                                                (uiop:getenv
                                                                 "HOME")
                                                                (subseq s 1)))
                                               (uiop:getenv "HOME")))
                                          (t s)))
                                  splits))))
               (tok-spaces (line)
                 ;; (format t "~%~S~%" line)
                 (let ((splits (tokenize-for-char line #\space :join-using " ")))
                   (loop for split in splits
                         collect (replace-env split))))
               (tok-doublequote (line)
                 (let ((toks (tokenize-double-quote line)))
                   (loop for (a b) on toks by #'cddr
                         collect (tok-spaces a)
                         when b
                           collect (list :double-quote (replace-env b)))))
               (tok-single-quote (line)
                 (let ((toks (tokenize-single-quote line)))
                   (loop for (a b) on toks by #'cddr
                         collect (tok-doublequote a)
                         when b
                           collect (list :single-quote b)))))
        (let ((tokenized (alexandria:flatten (tok-single-quote line))))
          (values
           (do* ((pipes nil)
                 (curpipe nil)
                 (tokens (alexandria:flatten (tok-single-quote line))
                         (cdr tokens))
                 (current-token (car tokens)
                                (car tokens)))
                ((null tokens) (reverse (remove-if (geneq #'string= "") pipes)))
             (if (keywordp current-token)
                 (progn (setf tokens (cdr tokens))
                        (push (car tokens) pipes))
                 (let ((splits (splitter current-token)))
                   (loop for tok in splits
                         do (push tok pipes)))))
           tokenized)))
    (abord-tokenization () nil)))

(defun split-for-pipelining (tokens)
  (let ((pipes nil)
        (curpipe nil))
    (map nil
         (lambda (token)
           (cond ((and (stringp token)
                       (string= token "|"))
                  (push (reverse curpipe) pipes)
                  (setf curpipe nil))
                 ((and (stringp token)
                       (string= token "<"))
                  (push (reverse curpipe) pipes))
                 ((and (stringp token)
                       (string= token ">>"))
                  (push (reverse curpipe) pipes)
                  (setf curpipe (list :append-to-file)))
                 ((and (stringp token)
                       (string= token ">"))
                  (push (reverse curpipe) pipes)
                  (setf curpipe (list :output-to-file)))
                 ((and (null curpipe)
                       (listp token)
                       (keywordp (car token)))
                  (push token pipes))
                 (t
                  (push token curpipe))))
         tokens)
    (push (reverse curpipe) pipes)
    (reverse pipes)))

;;; READING

;;; When reading, we can read with a simple algorithm by utilizing the errors
;;; reported when parsing/tokenizing.
;;;
;;; 1. read a line, and append it to the existing line if it exists.
;;; 2. if the line ends in a #\\ character, goto 1.
;;; 3. attempt to parse/tokenize the line.
;;; 4. if tokenization fails due to our miscount errors, goto 1. 
;;; 5. pass tokenized line to split-for-pipelining.

;; Lisp readers

(defmacro define-lisp-reader (character (count stream-name) &body body)
  (let ((fn (intern (format nil "~C-LISP-READER" character)
                    (find-package :sbcl-shell))))
    `(progn
       (defun ,fn (,count ,stream-name) ,@body)
       (setf (gethash ,character *lisp-readers*) #',fn))))

(define-lisp-reader #\& (count stream)
  (list :lisp-subshell
        (cons 'progn (loop repeat count collect (with-package ()
                                                  (read stream))))))

(defun call-lisp-reader (num char stream)
  (let ((fn (gethash char *lisp-readers*)))
    (funcall fn num stream)))

;; Utilities
(defun number-char-p (char)
  (member char '(#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9) :test #'char=))

(defun read-num (stream)
  (loop for c = (peek-char nil stream)
        if (number-char-p c)
          collect (read-char stream) into n
        else
          do (return-from read-num
               (or (ignore-errors (parse-integer (coerce n 'string)))
                   1))))

(define-condition tokread-add-new-line (error) ())

(defun read-it (stream)
  (let (ac)
    (loop for char = (read-char stream nil nil)
          while char
          if (char= char #\")
            do (push char ac)
               (loop named dblq
                     for char = (handler-case (read-char stream)
                                  (end-of-file ()
                                    (error 'tokread-add-new-line)))
                     if (char= char #\")
                       do (push char ac)
                          (return-from dblq nil)
                     else if (char= #\\ char)
                            do (push (read-char stream) ac)
                     else do (push char ac))
          else if (char= char #\')
                 do (push char ac)
                    (loop named slq
                          for char = (handler-case (read-char stream)
                                       (end-of-file ()
                                         (error 'tokread-add-new-line)))
                          if (char= char #\')
                            do (push char ac)
                               (return-from slq nil)
                          else if (char= #\\ char)
                                 do (push (read-char stream) ac)
                          else do (push char ac))
          else if (char= char #\#)
                 do (let* ((next (peek-char nil stream))
                           (int (if (number-char-p next)
                                    (read-num stream)
                                    1))
                           (dispatch-char (read-char stream)))
                      (push (call-lisp-reader int dispatch-char stream) ac))
          else if (char= #\\ char)
                 do (push (read-char stream) ac)
          else
            do (push char ac))
    (reverse ac)))

(defun read-total-line (prompt)
  (let ((total-line (cl-readline:readline :prompt prompt :add-history t)))
    (if total-line 
        (labels
            ((filler ()
               (with-input-from-string (s total-line)
                 (handler-case (read-it s)
                   (tokread-add-new-line ()
                     (when *dbg*
                       (format t "~%ERRORED ON TOKREAD-ADD-NEW-LINE~%"))
                     (setf total-line
                           (concatenate 'string
                                        total-line
                                        #.(coerce '(#\newline) 'string)
                                        (cl-readline:readline :prompt "> "
                                                              :add-history t)))
                     (filler))
                   (error ()
                     (when *dbg*
                       (format t "~%ERRORED ON GENERIC ERROR~%"))
                     (setf total-line
                           (concatenate 'string
                                        total-line
                                        (cl-readline:readline :prompt "> "
                                                              :add-history t)))
                     (filler))))))
          (filler))
        (error 'end-of-file :stream *standard-input*))))

(defun read-shell-line (prompt)
  (flet ((stringify (list)
           (flet ((pred (a b)
                    (and (or (characterp a) (stringp a))
                         (or (characterp b) (stringp b))))
                  (joinery (a b)
                    (let ((sa (if (characterp a) (coerce (list a) 'string) a))
                          (sb (if (characterp b) (coerce (list b) 'string) b)))
                      (concatenate 'string sa sb))))
             (join #'pred #'joinery list))))
    (let ((bg nil))
      (values 
       (remove-if #'null
                  (split-for-pipelining
                   (loop for el in (stringify
                                    (let* ((charlist (read-total-line prompt))
                                           (lastchar (car (last charlist))))
                                      (when *dbg*
                                        (format t "lastchar: ~A, charlist: ~A~%"
                                                lastchar charlist))
                                      (if (and (characterp lastchar)
                                               (char= lastchar #\&))
                                          (progn
                                            (setf bg t)
                                            (butlast charlist))
                                          charlist)))
                         if (stringp el)
                           append (alexandria:flatten (tokenize-line el))
                         else
                           collect el)))
       bg))))

