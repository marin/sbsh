(in-package :sbcl-shell)

(defvar *debug-on-error* t
  "Enter the debugger on error")

(defvar *dbg* nil
  "General debug variable.")

(defvar *debug-stream* nil)

(defvar *dfmt-keys* ()
  "A set of keys controlling which debug statements are printed")

(defvar *dfmt-everything* nil
  "When T ignore dfmt keys and write every debug statement")

(defvar *previous-cmd-exit-code* 0
  "Hold the result of the previous pipeline")

(defvar *read-package* (find-package :sbcl-shell))

(defparameter *token-terminating-characters* "'\"|&<> ")

(defparameter *shell-var-name-chars*
  '#.(coerce "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_" 'list))

(defparameter *shell-variables* (make-hash-table :test #'equal))

(defvar *builtin-hash* (make-hash-table :test #'equal)
  "A hash table of builtins")
(defvar *alias-hash*   (make-hash-table :test #'equal)
  "A hash table of aliases")
(defvar *always-run-in-subshell* (make-hash-table :test #'equal)
  "hash table of everything that must run in a subshell")

(defmacro flet-def-and-call ((args function call-with &key (dynamic-extent t))
                             &body body)
  "Generate a single local function with argument list ARGS using flet and apply
FUNCTION with this function as its first argument and CALL-WITH as its remaining
arguments.

Used for defining with-resource macros."
  (let ((f (gensym "CONT")))
    `(flet ((,f ,args ,@body))
       ,@(when dynamic-extent
           `((declare (dynamic-extent #',f))))
       (,function #',f ,@call-with))))

(define-condition invalid-shell-spec-error (error)
  ((spec :initarg :spec
         :reader invalid-shell-spec-error-spec
         :initform 'not-provided))
  (:report
   (lambda (c s)
     (format s "Invalid spec ~S~&"
             (invalid-shell-spec-error-spec c))
     (force-output s))))

(defmacro geneq (func arg1)
  `(lambda (el)
     (funcall ,func ,arg1 el)))

(defmacro with-package ((&optional (package nil ppp)) &body body)
  `(let ((*package* (find-package ,(cond (ppp package)
                                         (package package)
                                         (t '*read-package*)))))
     ,@body))

(defun dfmt (key control-string &rest args)
  "Format to the debug output stream when KEY is present in *DFMT-KEYS*. KEY may
be a single key or a list of keys. If it is a list of keys and the first element
is :AND then all elements of KEY must be present in *DFMT-KEYS* for the message
to be formatted. If the first element is :OR or any other key then if any
element is present in *DFMT-KEYS* the message will be formatted. If KEY is the
object T then the message is always formatted."
  (when (or *dfmt-everything*
            (eq key t)
            (and (listp key)
                 (cond ((eql (car key) :and)
                        (null (set-difference (cdr key) *dfmt-keys*)))
                       (t ;; car is :or, also default
                        (intersection key *dfmt-keys*))))
            (member key *dfmt-keys*))
    (apply #'format *debug-stream* control-string args)
    (force-output *debug-stream*)))

(defun invoke-with-debug-file (file-output-stream fn)
  (let ((*debug-stream* file-output-stream))
    (funcall fn file-output-stream)))

(defmacro with-debug-file ((var filespec &key (if-exists :append)) &body body)
  (let ((fn (gensym)))
    `(flet ((,fn (,var)
              ,@body))
       (declare (dynamic-extent #',fn))
       (with-open-file (,var ,filespec :direction :output
                                       :if-exists ,if-exists
                                       :if-does-not-exist :create)
         (invoke-with-debug-file ,var #',fn)))))

(defun join (p j l)
  "Join elements of a list, L, together. P is a predicate which will be called
with the two elements to consider joining. If it returns NIL, JOIN moves to the
next element in the list. If P returns T then J is called with the two elements,
and the initial element of the list is replaced with the return value."
  (cond ((null l)
         nil)
        ((funcall p (car l) (cadr l))
         (join p j (cons (funcall j (car l) (cadr l))
                         (cddr l))))
        (t
         (cons (car l) (join p j (cdr l))))))

(defun butlast-char (string)
  (subseq string 0 (1- (length string))))

(defun ensure-stream-fd (stream)
  (cond ((sb-sys:fd-stream-p stream)
         (sb-sys:fd-stream-fd stream))
        ((typep stream 'synonym-stream)
         (ensure-stream-fd (synonym-stream-symbol stream)))
        ((typep stream 'symbol)
         (ensure-stream-fd (symbol-value stream)))
        (t (error "Unknown stream type ~A" stream))))

(defun bang-arg-p (s)
  (and (symbolp s)
       (> (length (symbol-name s)) 1)
       (string= (symbol-name s)
                "!"
                :start1 0
                :end1 1)
       (numberp (ignore-errors (parse-integer (subseq (symbol-name s) 1))))))

(defun bang-arg-number (s)
  (parse-integer (subseq (symbol-name s) 1)))

(defun make-bang-arg (integer)
  (intern (format nil "!~D" integer)))

(defun generate-bang-arg-symbol-list (number-of-bang-args)
  (loop for i from 1 to number-of-bang-args collect (make-bang-arg i)))

(defmacro with-bang-vars ((&optional (read-function 'read))&body body)
  (let* ((vars (sort (remove-duplicates (remove-if-not #'bang-arg-p
                                                       (alexandria:flatten body)))
                     #'> :key 'bang-arg-number))
         (symlist (generate-bang-arg-symbol-list (bang-arg-number (car vars))))
         (rfn (gensym)))
    `(flet ((,rfn ()
              (let ((rcase (readtable-case *readtable*))
                    (read-fn ,(if (symbolp read-function)
                                  `(function ,read-function)
                                  read-function)))
                (unwind-protect (progn
                                  (setf (readtable-case *readtable*) :preserve)
                                  (funcall read-fn))
                  (setf (readtable-case *readtable*) rcase)))))
       (let* ,(mapcar (lambda (s) `(,s (funcall #',rfn))) symlist)
         (declare (ignorable ,@symlist))
         (locally ,@body)))))

(defun |bang-var-reader| (stream subchar count)
  (declare (ignore subchar count))
  (let ((read-in (read stream)))
    (if (symbolp read-in)
        (let ((fn read-in)
              (read-in (read stream)))
          `(with-bang-vars (,fn)
             ,read-in))
        `(with-bang-vars () ,read-in))))
