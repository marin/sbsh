(in-package :sbcl-shell)

(defvar *lisp-readers* (make-hash-table))
(defvar *in-lisp-reader-p* nil)
(defvar *expansion-logfile* "~/.sbsh-expansions.txt")

(defun number-char-p (char)
  (member char '(#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9) :test #'char=))

(defun gather-path-elements (&optional filter)
  "Gather all files present in the path, optionally filtering them with FILTER
prefix."
  (flet ((path-file (path)
           (let ((type (pathname-type path))
                 (name (pathname-name path)))
             (concatenate 'string name (when type ".") (when type type)))))
    (let ((path (cl-ppcre:split ":" (uiop:getenv "PATH"))))
      (loop for %dir in path
            for dir = (make-pathname :directory %dir)
            append (if filter
                       (loop for file in (uiop:directory-files dir)
                             when (uiop:string-prefix-p filter (path-file file))
                               collect (path-file file))
                       (mapcar #'path-file (uiop:directory-files dir)))))))

(cffi:defcfun ("filename_completion_function" complete-filename) :string
  "complete for filenames"
  (text :string)
  (state :int))

(defun longest-common (s1 s2 &optional end)
  (do ((i 0 (1+ i)))
      ((or (and end (= i end))
           (not (ignore-errors (char= (char s1 i) (char s2 i)))))
       i)))

(defun longest-prefix (list)
  (let ((prefix (car list))
        (i (length (car list))))
    (loop for string in (cdr list)
          for index = (longest-common prefix string i)
          when (< index i)
            do (setf prefix (subseq string 0 index)
                     i index))
    prefix))

;; This function is very naive, it doesnt check for truly being in a lisp
;; subsection. For example, echo "#&(form" will try to complete lisp. 
(defun in-lisp-reader-p (word start end
                         &optional (buffer cl-readline:*line-buffer*))
  (declare (ignore word))
  (let ((buflen (length buffer))
        (stream nil))
    (do ((i 0 (1+ i)))
        ((= i buflen) nil)
      (format stream "~D: ~C~&" i (char buffer i))
      (when (> i end)
        (return-from in-lisp-reader-p nil))
      (when (char= (char buffer i) #\#)
        (format stream "In reader~&")
        (block inlisp
          (do ((j (1+ i) (1+ j))
               (c 0 c))
              ((or (and (= j start)
                        (return-from in-lisp-reader-p t))
                   (= j buflen)))
            (format stream "J: ~D: ~C~&" j (char buffer j))
            (when (char= #\( (char buffer j))
              (format stream "Found opening paren~&")
              (incf c))
            (when (char= #\) (char buffer j))
              (if (= c 1)
                  (if (<= i start end j)
                      (return-from in-lisp-reader-p t)
                      (progn (setf i j)
                             (return-from inlisp nil)))
                  (decf c)))))))))

(defun gen-lisp-comps (string)
  (car (slynk:simple-completions string (find-package :sbcl-shell))))

(defun expander-function (word start end)
  "Attempt to complete binary names in the path"
  (when (or *dfmt-everything*
            (member :readline-completion *dfmt-keys*))
    (with-open-file (f *expansion-logfile* :if-exists :append
                                           :if-does-not-exist :create
                                           :direction :output)
      (format f "Expanding (~D, ~D) ~S in ~S~&"
              start end word cl-readline:*line-buffer*)))
  (flet ((complete-from-path ()
           (and (not (and (> (length word) 2)
                          (char= (char word 0) #\.)
                          (char= (char word 1) #\/)))
                (or (zerop start)
                    (loop named check-for-cmd
                          for i from (- start 1) downto 0
                          for char = (char cl-readline:*line-buffer* i)
                          if (char= char #\|)
                            do (return-from check-for-cmd t)
                          else if (not (sb-unicode:whitespace-p char))
                                 do (return-from check-for-cmd nil)
                          finally (return-from check-for-cmd t)))))
         (gen-file-comps ()
           (let ((comps nil))
             (do* ((state 0 1)
                   (completion (complete-filename word state)
                               (complete-filename word state)))
                  ((null completion)
                   (reverse comps))
               (push completion comps))))
         (gen-path-comps ()
           (remove-duplicates
            (gather-path-elements (subseq cl-readline:*line-buffer* start end))
            :test #'string=)))
    (let* ((comps (cond ((in-lisp-reader-p word start end)
                         (gen-lisp-comps (subseq cl-readline:*line-buffer*
                                                 start
                                                 end)))
                        ((complete-from-path)
                         (gen-path-comps))
                        (t (gen-file-comps))))
           (longest-prefix (longest-prefix comps)))
      (if (cdr comps)
          (cons longest-prefix comps)
          comps))))

(defun lookup-shell-variable (var)
  (dfmt '(:variable-lookup :environment-variable :shell-variable)
        "~&LOOKUP VARIABLE: ~A~&" var)
  (let ((v (gethash var *shell-variables*)))
    (or v
        (uiop:getenv var)
        "")))

(defun replace-and-conjoin (replace-in replace-with start &optional end)
  (let ((a (subseq replace-in 0 start))
        (b replace-with)
        (c (if end (subseq replace-in end) "")))
    (values (concatenate 'string a b c)
            (+ start (length replace-with)))))

(defun expand-for-$ (line)
  (dfmt '(:shell-expansion :environment-variable :shell-variable)
        "~&Expanding ~S for $~&" line)
  (let ((prev-$ nil))
    (flet ((replacement (i)
             (let ((var (subseq line (1+ prev-$) i)))
               (multiple-value-bind (newline newi)
                   (replace-and-conjoin line
                                        (lookup-shell-variable var)
                                        prev-$
                                        i)
                 (setf line newline
                       prev-$ nil)
                 (1- newi)))))
      (loop named expander
            for i from 0
            do (let ((c (ignore-errors (char line i))))
                 (cond (c
                        (when (and prev-$
                                   (not (member c *shell-var-name-chars*
                                                :test #'char=)))
                          (let ((newi (replacement i)))
                            (setf i (if (char= c #\$)
                                        (1+ newi)
                                        newi))))
                        (when (and (char= c #\$)
                                   (not (ignore-errors
                                         (char= #\\ (char line (1- i))))))
                          (setf prev-$ i)))
                       (t (return-from expander (when prev-$
                                                  (replacement i)))))))))
  (dfmt '(:shell-expansion :environment-variable :shell-variable)
        "~&Expanded into ~S~&" line)
  line)

(defun discard-whitespace (stream)
  (let (c)
    (tagbody
     peek
       (setf c (peek-char nil stream))
     check
       (when (sb-unicode:whitespace-p c)
         (read-char stream)
         (go peek))))
  (values))

(defun read-quotes (char stream &aux (tok nil))
  (loop named loc
        for n = (read-char stream)
        if (char= #\\ n)
          do (push (read-char stream) tok)
        else if (char= char n)
               do (return-from loc nil)
        else
          do (push n tok))
  (if (char= char #\")
      (expand-for-$ (coerce (reverse tok) 'string))
      (coerce (reverse tok) 'string)))

(defun read-expand-$ (stream)
  (let ((tok (loop for c = (read-char stream nil nil)
                   while (member c *shell-var-name-chars*)
                   collect c
                   finally (when c (unread-char c stream)))))
    (lookup-shell-variable (coerce tok 'string))))

;;; Lisp Readers

(defmacro define-lisp-reader (character (count stream-name) &body body)
  (let ((fn (intern (format nil "~C-LISP-READER" character)
                    (find-package :sbcl-shell))))
    `(progn
       (defun ,fn (,count ,stream-name) ,@body)
       (setf (gethash ,character *lisp-readers*) #',fn))))

(defun call-lisp-reader (num char stream)
  (let ((fn (gethash char *lisp-readers*))
        (*in-lisp-reader-p* t))
    (funcall fn num stream)))

(define-lisp-reader #\% (count stream) ; #&, #3&, etc.
  (list :lisp-subshell
        (cons 'progn (loop repeat count collect (with-package ()
                                                  (read stream))))))

(define-lisp-reader #\& (count stream)
  (discard-whitespace stream)
  (let ((res (peek-char nil stream)))
    (if (char= res #\")
        (setf res (read stream))
        (setf res (read-char stream)))
    (list :lisp-subshell-format-result
          (cond ((stringp res) res)
                ((char= res #\;) nil)
                ((char= res #\~) 'pprint)
                (t (format nil "~~0&~~~C~~&" res)))
          (cons 'progn (loop repeat count collect (with-package ()
                                                    (read stream)))))))

(defun read-num (stream)
  (loop for c = (peek-char nil stream)
        if (number-char-p c)
          collect (read-char stream) into n
        else
          do (return-from read-num
               (or (ignore-errors (parse-integer (coerce n 'string)))
                   1))))

(defun read-one-token (stream)
  (let ((char (read-char stream nil nil)))
    (when char
      (cond ((or (char= char #\") (char= char #\'))
             (let ((tok (read-quotes char stream)))
               (if (char= char #\")
                   (expand-for-$ tok)
                   tok)))
            ((char= char #\#)
             (let* ((next (peek-char nil stream))
                    (int (if (number-char-p next)
                             (read-num stream)
                             1))
                    (dispatch-char (read-char stream)))
               (call-lisp-reader int dispatch-char stream)))
            (t
             (let ((c char)
                   (tok nil))
               (tagbody
                  (go begin)
                obtain
                  (setf c (handler-case (read-char stream)
                            (end-of-file ()
                              (go _end))))
                begin
                  (let ((flag nil))
                    (cond ((char= #\\ c)
                           (setf c (read-char stream)))
                          ((char= #\newline c)
                           (go end))
                          ((member c (coerce *token-terminating-characters*
                                             'list)
                                   :test #'char=)
                           (go end))
                          ((char= c #\$)
                           (setf tok
                                 (append (reverse (coerce (read-expand-$ stream)
                                                          'list))
                                         tok))
                           (setf flag t)))
                    (when (null flag)
                      (push c tok))
                    (go obtain))
                end
                  (if (null tok)
                      (when (member c '(#\< #\> #\| #\&) :test #'char=)
                        (let ((next-char (peek-char nil stream)))
                          (if (char= next-char c)
                              (progn (read-char stream)
                                     (setf tok (list c c)))
                              (setf tok (list c)))))
                      (unread-char c stream))
                _end)
               (let ((str (coerce (reverse tok) 'string)))
                 (if (and (car tok) (char= (car tok) #\=))
                     (concatenate 'string str (read-one-token stream))
                     str))))))))

(defun read-all-tokens (stream)
  (loop for tok = (read-one-token stream)
        while tok
        unless (and (stringp tok) (string= tok ""))
          collect tok))

(defun obtain (p)
  (let ((total-line (cl-readline:readline :prompt p
                                          :add-history t
                                          :already-prompted t)))
    (if total-line
        (labels ((filler ()
                   (with-input-from-string (s total-line)
                     (handler-case (read-all-tokens s)
                       (end-of-file ()
                         (setf total-line
                               (concatenate 'string
                                            total-line
                                            (cl-readline:readline :prompt "> "
                                                                  :add-history t)))
                         (filler))))))
          (filler))
        (error 'end-of-file :stream *standard-input*))))

(defun obtain-tokens (in-stream out-stream)
  (declare (ignorable in-stream out-stream))
  (let ((string (with-output-to-string (s)
                  (write-prompt s))))
    (write-string string out-stream)
    (finish-output out-stream)
    (obtain string)))
