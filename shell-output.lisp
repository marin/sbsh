(in-package :sbcl-shell)

(defgeneric format-terminal-control-spec (stream spec opt &key &allow-other-keys))

(defmethod format-terminal-control-spec (stream
                                         (spec (eql :foreground))
                                         (opt number) &key &allow-other-keys)
  (format stream "38;2;~D;~D;~D"
          (ldb (byte 8 16) opt)
          (ldb (byte 8 8) opt)
          (ldb (byte 8 0) opt)))

(defmethod format-terminal-control-spec (stream
                                         (spec (eql :foreground))
                                         (opt symbol) &key &allow-other-keys)
  (format stream "3~D"
          (case opt
            ((:black) 0) ((:red) 1)     ((:green) 2) ((:yellow) 3)
            ((:blue) 4)  ((:magenta) 5) ((:cyan) 6)  ((:white) 7)
            (otherwise
             (error "~A is not a valid color" opt)))))

(defmethod format-terminal-control-spec (stream
                                         (spec (eql :background))
                                         (opt number) &key &allow-other-keys)
  (format stream "48;2;~D;~D;~D"
          (ldb (byte 8 16) opt)
          (ldb (byte 8 8) opt)
          (ldb (byte 8 0) opt)))

(defmethod format-terminal-control-spec (stream
                                         (spec (eql :background))
                                         (opt symbol) &key &allow-other-keys)
  (format stream "4~D"
          (case opt
            ((:black) 0) ((:red) 1)     ((:green) 2) ((:yellow) 3)
            ((:blue) 4)  ((:magenta) 5) ((:cyan) 6)  ((:white) 7)
            (otherwise
             (error "~A is not a valid color" opt)))))

(defmethod format-terminal-control-spec (stream (spec (eql :bold)) opt
                                         &key &allow-other-keys)
  (if opt
      (format stream "1")
      (format stream "22")))

(defmethod format-terminal-control-spec (stream (spec (eql :faint)) opt
                                         &key &allow-other-keys)
  (if opt
      (format stream "2")
      (format stream "22")))

(defmethod format-terminal-control-spec (stream (spec (eql :italic)) opt
                                         &key &allow-other-keys)
  (if opt
      (format stream "3")
      (format stream "23")))

(defmethod format-terminal-control-spec (stream (spec (eql :underline)) opt
                                         &key &allow-other-keys)
  (if opt
      (format stream "4")
      (format stream "24")))

(defmethod format-terminal-control-spec (stream (spec (eql :blinking)) opt
                                         &key &allow-other-keys)
  (if opt
      (format stream "5")
      (format stream "25")))

(defmethod format-terminal-control-spec (stream (spec (eql :reverse)) opt
                                         &key &allow-other-keys)
  (if opt
      (format stream "7")
      (format stream "27")))

(defun format-terminal-controls (stream &rest specs)
  (format stream "~%Outputting options: ~S~%" specs)
  (format stream "~C[" #\esc)
  (loop for (spec . rest) on specs
        do (when spec
             (apply 'format-terminal-control-spec stream spec)
             (when rest (format stream ";"))))
  (format stream "m")
  (finish-output stream))

(defun reset-terminal (stream)
  (format stream "~C~C[0m~C" #\SOH #\ESC #\STX)
  (finish-output stream))

(defun invoke-with-ansi-options (continuation stream &rest options)
  (dfmt :ansi-options "~%OUTPUTING ASCII OPTIONS: ~S~%" options)
  (unwind-protect (progn
                    (format stream "~C~C[" #\SOH #\ESC)
                    (loop for (spec opt . rest) on options by #'cddr
                          do (format-terminal-control-spec stream spec opt)
                             (when rest (format stream ";"))
                          finally (format stream "m~C" #\STX))
                    (funcall continuation stream))
    (reset-terminal stream)))

(defmacro with-ansi-options ((var stream &rest options) &body body)
  `(flet-def-and-call ((,var) invoke-with-ansi-options (,stream ,@options))
     ,@body))

