LISP := $(if $(LISP),$(LISP),sbcl)

all: mksbsh

mksbsh: mksbclsh
	./sbclsh --eval "(sb-ext:save-lisp-and-die \"sbsh\" :executable t :toplevel (lambda () (sbcl-shell::run-shell)))"
#	./sbclsh --eval "(sb-ext:save-lisp-and-die \"sbsh\" :executable t :toplevel (lambda () (sbcl-shell::init-shell) (sbcl-shell::run-shell)))"

mksbclsh: genlib
	$(LISP) --load "./sbcl-shell.asd" --eval "(asdf:load-system :sbcl-shell)" --eval "(sb-ext:save-lisp-and-die \"sbclsh\" :executable t)"

genlib: expose-exec.c
	gcc -shared -Wall -o sbsh_lib expose-exec.c

install: sbsh sbsh_lib # installlib installsbsh
	cp sbsh_lib /usr/local/bin
	cp sbsh /usr/local/bin
